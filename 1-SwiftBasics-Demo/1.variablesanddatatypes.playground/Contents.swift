
// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.

//: Playground - noun: a place where people can play
// I learnt how to use variables and data types in this playground

import UIKit

// keyword
var str = "Hello, playground"

var course: String = "Total iOS Blueprint"
course = "Socialize Your Apps"
course

var language: String = "Swift"
var software: String = "Xcode"
var programmingIsFun: Bool = true
var version: Int = 19

language = "Objective-C"
version = 09

var price: Double = 199.9999















