

// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.

//: Playground - noun: a place where people can play
// String and String Interpolation

import UIKit

var str = "Hello, playground"           // String

var name: String = "Duc"
var verb: String = "loves"
var noun = "Swift"                  // infer, String

name = "Tracy"
verb = "excels"

let newSentence = name + " " + verb + " at " + noun


let myFaceBookPage = "Duc Tran"
let numberOfLikes = 10000
let owner = "their"

"Duc Tran has 10000 likes on his facebook page"

let text = "\(myFaceBookPage) has \(numberOfLikes) likes on \(owner) facebook page"









