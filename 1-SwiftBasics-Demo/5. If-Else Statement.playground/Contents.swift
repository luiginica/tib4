
// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.


// If - Else Statement in Swift

import UIKit

var myName = "Duc Tran"
let numberOfCharacters = myName.characters.count
let isLongName = numberOfCharacters > 10

if isLongName {
    print("you have a long name")
    myName = "You name is DucTran"
} else {
    print("oh, it's okay")
}

print("continue with the program")


var groceryList = ["broccoli", "noodles", "beef", "chicken"]
let newItem = "chicken"

if groceryList.contains(newItem) {
    print("you already added the item to the list")
} else {
    groceryList.append(newItem)
    print("item added. your new shopping list: \(groceryList)")
}













