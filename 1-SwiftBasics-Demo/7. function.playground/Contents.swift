
// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.


let grade1 = 90
let grade2 = 88
let grade3 = 93

let sumGrades = grade1 + grade2 + grade3
let avgGrades = Double(sumGrades) / 3.0

// *****

let age1 = 20
let age2 = 39
let age3 = 2

let avgAges = Double(age1 + age2 + age3) / 3.0

// tates 3 number, and return their average
func average(n1: Int, n2: Int, n3: Int) -> Double
{
    let sum = n1 + n2 + n3
    let average = Double(sum) / 3.0
    
    return average
}

let avg = average(n1: 9, n2: 8, n3: 2)


let numbers: [Int] = [2, 3, 4, 5, 6, 7, 9,2, 3, 4, 5, 2, 3, 4, 5, 6, 7, 9,2, 3, 4, 5, 2, 3, 4, 5, 6, 7, 9,2, 3, 4, 5, 2, 3, 4, 5, 6, 7, 9,2, 3, 4, 5, 2, 3, 4, 5, 6, 7, 9,2, 3, 4, 5, 2, 3, 4, 5, 6, 7, 9,2, 3, 4, 5]


func average(numbers: [Int]) -> Double
{
    let count = numbers.count
    let lastIndex = count - 1
    var sum = 0
    
    for i in 0...lastIndex {
        sum += numbers[i]
    }
    
    return Double(sum) / Double(count)
}

let avg2 = average(numbers: numbers)














