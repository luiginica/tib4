
// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.

//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

var billBeforeTax: Double = 50.0
var taxPercentage: Double = 6.0
var numberOfPeople: Int = 2

billBeforeTax = 59.0

let tipPercentage: Double = 15.0
let taxAmount: Double = billBeforeTax * taxPercentage / 100.0
let totalBill: Double = billBeforeTax + taxAmount
let tipAmount: Double = totalBill * tipPercentage / 100.0

let tip2: Double = (billBeforeTax + taxAmount) * tipPercentage / 100.0











