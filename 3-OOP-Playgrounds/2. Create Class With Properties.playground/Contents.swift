//: Playground - noun: a place where people can play

import UIKit

/*
 
 + Opp:
 Build a social network app that has things like user, post, newsfeed, photo, video... Right now there's no way we can do that with simple data type like String, Int... because each data type itself can't construct the real-world object.
 + Challenge:
 Unfortunately right now, with variable, constant, or array, there's no way for us to create one because an album has lots of information.
 
 + Solution:
 In programming, we refer to the effort of modeling real life objects as object oriented programming.
 We can do that in Swift by creating a data structure.
 */

/*
 
 3 Takeaways:
 (1) It is just a blueprint. It doesn't contain any specific or concrete data. From this blueprint (data structure), we can create instances of the class.
 (2) We use these structures because we want to encapsulate data - group multiple properties and functionalities into one single structure.
 (3) A class can contain properties, we call it stored properties that are variables and constants. We need to explicitly declare their types because these properties didn't have any initial values. These properties don't exist outside the scope of the structure. They only exist in the actual thing, the instance we will create.
 
 */

class User {
    var username: String = ""
    var fullName: String = ""
    var email: String = ""
}

// * Code Challenge

class Post {
    var text: String = ""
    var username: String = ""
}
















