/*
 
 Opp:
 When we work with data in programming, we don't just want to store it there. We also want to process it, manipulate it, does something with it. That's the job of a function.
 
 Challenge:
 Right now we don't have any functionalities yet. Let's add some to our class. We call these functionalities method.
 
 */

/*
 3 Points:
 (1) To create a method, we ...
 (2) Method is a function associated with a type. We call it differently to distinguish between stand-alone function and the one associated with a type.
 (3) We learnt how to create an instance method. We cal it instance method because they can only be called on an instance.
 */

import Foundation

class User {
    var username: String
    var fullName: String
    var email: String
    var posts: [Post] = []       // D2.1
    
    init(username: String, fullName: String, email: String) {
        self.username = username
        self.fullName = fullName
        self.email = email
    }
    
    // D1
    func signupSuccessfully() -> Bool {
        print("Making sign up request to our server")
        print("...")
        print("Sign up successfully!")
        return true
    }
    
    // D1.2
    func loginSuccessfully() -> Bool {
        print("Making request to login user with username: \(username) and email: \(email)")
        print("...")
        print("Login successfully!")
        return true
    }
    
    // D2.2
    func post(newPost: Post) {
        self.posts.insert(newPost, at: 0)
    }
}

// * Code Challenge with C*

class Post {
    var text: String
    var username: String
    var likers: [User] = []      // C2.1
    var comments: [Comment] = []    // C3.1
    
    init(text: String, byUserWithUsername username: String) {
        self.text = text
        self.username = username
    }
    
    // C2.2
    func liked(by user: User) {
        self.likers.append(user)
    }
    
    // C3.2
    func comment(with comment: Comment) {
        self.comments.append(comment)
    }
    
    // C4
    func getAllLikers() -> String {
        var result = ""
        
        for liker in likers {
            if result == "" {
                result += liker.username
            } else {
                result += ", \(liker.username)"
            }
        }
        
        return result
    }
    
    // C5
    func getAllCommenters() -> String {
        var result = ""
        
        for comment in comments {
            let commenter = comment.user
            if result == "" {
                result += commenter.username
            } else {
                result += ", \(commenter.username)"
            }
        }
        
        return result
    }
    
    // C6
    /*
     Algorithm: if a post has n likes, it has n*5 points. If it has m comments, each comment that is positive has n*10 points. If the comment contains negative words ("dork, stupid, mean"), it gets n*(-3) points.
     */
    func getPoints() -> Int
    {
        var points = likers.count * 5
        
        for comment in comments {
            let text = comment.text.lowercased()
            if text.contains("dork") || text.contains("stupid") || text.contains("mean") {
                points -= 3
            } else {
                points += 10
            }
        }
        
        return points
    }
}


// C1

class Comment {
    let user: User
    var text: String
    
    init(user: User, text: String) {
        self.user = user
        self.text = text
    }
}

// C7 - Test
let currentUser = User(username: "ductran", fullName: "Duc Tran", email: "support@ductran.co")
let user1 = User(username: "steve", fullName: "Steve Jobs", email: "steve@apple.com")
let user2 = User(username: "tim", fullName: "Tim Cook", email: "tim@apple.com")
let user3 = User(username: "jony", fullName: "Jony Ive", email: "jony@apple.com")

let newPost = Post(text: "Hey there, it's Duc here!", byUserWithUsername: currentUser.username)
newPost.text = "woww!"
newPost.liked(by: user1)
newPost.liked(by: user2)
newPost.liked(by: user3)

let comment1 = Comment(user: user1, text: "Hey there this is a test comment")
newPost.comment(with: comment1)
let comment2 = Comment(user: user2, text: "You are mean :p")
newPost.comment(with: comment2)

newPost.getPoints()













