/*
 3 Points:
 (1) To use a data structure, we create concrete / specific instance of the data structure with data
 (2) We use a special method Swift creates for us to make a specific object with data assigned to its properties
 (3) To access the stored properties of an instance, we use dot notation
 
 */


// Q: A data structure represents an object too. An object has functionalities. How can we add functionalities to this structure?

class User {
    var username: String = ""
    var fullName: String = ""
    var email: String = ""
}

// * Code Challenge

// (C1)
class Post {
    var text: String = ""
    var username: String = ""
}

let currentUser = User()
currentUser.email = "support@ductran.co"
currentUser.fullName = "Duc Tran"
currentUser.username = "dtran"

// (C2)
let newPost = Post()
newPost.text = "Hey there, it's Duc!"
newPost.username = currentUser.username




