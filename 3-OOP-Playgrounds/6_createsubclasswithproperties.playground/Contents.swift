import Foundation

class User {
    var username: String
    var fullName: String
    var email: String
    var posts: [Post] = []
    
    init(username: String, fullName: String, email: String) {
        self.username = username
        self.fullName = fullName
        self.email = email
    }

    func signupSuccessfully() -> Bool {
        print("Making sign up request to our server")
        print("...")
        print("Sign up successfully!")
        return true
    }
    
    func loginSuccessfully() -> Bool {
        print("Making request to login user with username: \(username) and email: \(email)")
        print("...")
        print("Login successfully!")
        return true
    }

    func post(newPost: Post) {
        self.posts.insert(newPost, at: 0)
    }
}

class Post {
    var text: String
    var username: String
    var likers: [User] = []
    var comments: [Comment] = []
    
    init(text: String, byUserWithUsername username: String) {
        self.text = text
        self.username = username
    }
    
    func liked(by user: User) {
        self.likers.append(user)
    }
    
    func comment(with comment: Comment) {
        self.comments.append(comment)
    }
    
    func getAllLikers() -> String {
        var result = ""
        
        for liker in likers {
            if result == "" {
                result += liker.username
            } else {
                result += ", \(liker.username)"
            }
        }
        
        return result
    }
    
    func getAllCommenters() -> String {
        var result = ""
        
        for comment in comments {
            let commenter = comment.user
            if result == "" {
                result += commenter.username
            } else {
                result += ", \(commenter.username)"
            }
        }
        
        return result
    }
    
    /*
     Algorithm: if a post has n likes, it has n*5 points. If it has m comments, each comment that is positive has n*10 points. If the comment contains negative words ("dork, stupid, mean"), it gets n*(-3) points.
     */
    func getPoints() -> Int
    {
        var points = likers.count * 5
        
        for comment in comments {
            let text = comment.text.lowercased()
            if text.contains("dork") || text.contains("stupid") || text.contains("mean") {
                points -= 3
            } else {
                points += 10
            }
        }
        
        return points
    }
}


class Comment {
    let user: User
    var text: String
    
    init(user: User, text: String) {
        self.user = user
        self.text = text
    }
}

let currentUser = User(username: "ductran", fullName: "Duc Tran", email: "support@ductran.co")
let user1 = User(username: "steve", fullName: "Steve Jobs", email: "steve@apple.com")
let user2 = User(username: "tim", fullName: "Tim Cook", email: "tim@apple.com")
let user3 = User(username: "jony", fullName: "Jony Ive", email: "jony@apple.com")

let newPost = Post(text: "Hey there, it's Duc here!", byUserWithUsername: currentUser.username)
newPost.text = "woww!"
newPost.liked(by: user1)
newPost.liked(by: user2)
newPost.liked(by: user3)

let comment1 = Comment(user: user1, text: "Hey there this is a test comment")
newPost.comment(with: comment1)
let comment2 = Comment(user: user2, text: "You are mean :p")
newPost.comment(with: comment2)

newPost.getPoints()

// ******************************
// *
// * (1) a class can subclass / inherit another class. We call the class a subclass or child class. The parent class is super class.
// * (2) The subclass has all of the super class's methods and stored properties
// * (3) We can add more properties to the subclass and they are only available for the instances of the subclass.
// *
// ******************************

class PhotoPost: Post {
    var photoURL: String = ""
}

let newPhotoPost = PhotoPost(text: "ABC", byUserWithUsername: user1.username)
newPhotoPost.photoURL = "http://www.ductran.io/testPhoto"



// * Code Challenge

class VideoPost: Post {
    var videoURL: String = ""
}

let someVidPost = VideoPost(text: "Sample caption", byUserWithUsername: user1.username)

class LinkPost: Post {
    var thumbnailURL: String = ""
    var linkURL: String = ""
    var linkTitle: String = ""
    var linkDescription: String = ""
}

let someLinkPost = LinkPost(text: "Sample link post", byUserWithUsername: user1.username)






