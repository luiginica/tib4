/*
 
 Opp:
 In this video, we'll talk about how to create our own initializer method and undertsand what's really going on under the hood when Swift creates one for us by default.
 
 */

class User {
    // (1) don't need initial values
    var username: String
    var fullName: String
    var email: String
    
    // (2) initializer is to initialize / give all properties of the class initial values
    init(username: String, fullName: String, email: String) {
        // (3) If we use username = username, it doesn't work because Swift will try to assign the user of this method to itself. We need to use the keyword self.
        self.username = username
        self.fullName = fullName
        self.email = email
    }
    
    // (4) What's a method? Why do we call it method? Next vid!
}

// * Code Challenge

class Post {
    var text: String
    var username: String
    
    // (C1)
    init(text: String, byUserWithUsername username: String) {
        self.text = text
        self.username = username
    }
}

let currentUser = User(username: "ductran", fullName: "Duc Tran", email: "support@ductran.co")

// (C2)
let newPost = Post(text: "Hey there, it's Duc here!", byUserWithUsername: currentUser.username)
newPost.text = "woww!"






















