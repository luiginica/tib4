

import Foundation

class UIColor {
    let hexNumber: String
    
    init(hex: String) {
        self.hexNumber = hex
    }
}

class UIImage
{
    var localFileName: String
    var width: Int = 500
    var height: Int = 500
    
    init(named name: String) {
        localFileName = name
    }
}

class UIView {
    var x: Int
    var y: Int
    var width: Int
    var height: Int
    var backgroundColor: UIColor = UIColor(hex: "#ffffff")
    var subViews = [UIView]()
    
    init(x: Int, y: Int, width: Int, height: Int) {
        self.x = x
        self.y = y
        self.width = width
        self.height = height
    }
    
    func add(subView: UIView) {
        self.subViews.insert(subView, at: 0)
    }

    func set(backgroundColor: UIColor) {
        self.backgroundColor = backgroundColor
    }
    
    func set(width: Int, height: Int) {
        self.width = width
        self.height = height
    }
    
    func set(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
}


class UIImageView : UIView {
    var image: UIImage
    
    init(image: UIImage) {
        self.image = image
        
        super.init(x: 0, y: 0, width: image.width, height: image.height)
    }
}

class UILabel : UIView {
    var text: String
    
    init(text: String, width: Int, height: Int) {
        self.text = text
    
        super.init(x: 0, y: 0, width: width, height: height)
    }
}

class UIButton : UIView {
    var title: String
    
    init(title: String, width: Int, height: Int) {
        self.title = title
        
        super.init(x: 0, y: 0, width: width, height: height)
    }
}


// Create the app UI

let mainView = UIView(x: 0, y: 0, width: 375, height: 667)
mainView.set(backgroundColor: UIColor(hex: "#48fc25"))

// Create an imageView and add it to the mainView
let mountainImage = UIImage(named: "mountain")
let myImageView = UIImageView(image: mountainImage)
mainView.add(subView: myImageView)
myImageView.x = 16
myImageView.y = 16
myImageView.set(width: 343, height: 300)

print(mainView.subViews)

// Create label with sample text
let sampleText = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in."
let textLabel = UILabel(text: sampleText, width: 343, height: 248)
textLabel.set(x: 16, y: 333)
mainView.add(subView: textLabel)

print(mainView.subViews)


let button = UIButton(title: "Click Me", width: 343, height: 54)
button.set(x: 16, y: 597)
mainView.add(subView: button)

print(mainView.subViews)












