
import UIKit

/**
 
 When you combine loop and if-else from the last video, things get really fancy!
 Let's try out two Code Challenges:
 
 Code Challenge 1:
 (1) Generate 200 random numbers in range of 0 to 10000
 (2) They have to be really random and put into an array called random numbers - camel case plz.
    
 * Hint:
    (1) You can use Int(arc4random()) to generate a random number
    (2) But Int(arc4random()) requires UIKit to be imported, write "import UIKit" in the beginning of your playground
    (3) And this random number out of the box can be somewhere from 0 to +2^31. We want only from 0 to 10000. To do this, you can use % - find the remainder of a division. work out the math here :)
 
 Code Challenge 2:
 (1) Find the minimum number from the 200 random numbers in the array you just created!
 
 * Hint:
    (1) This requires you to use... - well I won't reveal!
    (2) If you are finding for the largest integer in Swift, you can use Int(INT_MAX).
 
 
 
 
 
 */

var randomNumbers = [Int]()

for i in 1...200 {
    let randomNum = Int(arc4random()) % 10000   // becase we only want from 0 to 10000
    randomNumbers.append(randomNum)
}

randomNumbers
let lastIndex = randomNumbers.count - 1
var min = Int(INT_MAX)

for index in 0...lastIndex {
    let currentNumber = randomNumbers[index]
    
    if currentNumber < min {
        min = currentNumber
    }
}

min


// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.






