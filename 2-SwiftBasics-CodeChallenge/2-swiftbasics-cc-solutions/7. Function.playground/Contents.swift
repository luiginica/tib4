import Foundation

/**
 
 Function is what you'll use most of the time later on when we build apps in the next module.
 
 So let's have two challenges today!!
 
 CC1: Give you the bill before tax, tax percentage and tip percentage, can you write a function to return me the tip amount?
 CC2: Someone namely me gives you an array of integers, can you write a function named max, and returns me the maximum integer in this array?
 
 * Hint: there's nothing much I can give you in these CCs but if you are looking for the minimum integer, here it is: Int(INT16_MIN)
 
 */

// * Code Challenge 1

func calculateTipFor(billBeforeTax: Double, taxPercentage: Double, tipPercentage: Double) -> Double
{
    let tax = billBeforeTax * taxPercentage / 100.0
    let totalBill = billBeforeTax + tax
    
    return totalBill * tipPercentage / 100.0
}

// * Code Challenge 2

func max(numbers: [Int]) -> Int
{
    var max = Int(INT16_MIN)
    
    for i in 0..<numbers.count {
        if numbers[i] > max {
            max = numbers[i]
        }
    }
    
    return max
}


// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.









