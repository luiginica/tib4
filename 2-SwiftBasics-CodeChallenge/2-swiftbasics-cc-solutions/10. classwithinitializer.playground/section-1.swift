/**
 
 This time, let's add an initializer for our QuoteBook class giving it the initial quotes and authors.
 
 (1) If you are looking for the quotes and authors again, here they are:
        "When you knock on the door of opportunities, don't be surprise that it is work who answers.", "Stay Hungry, Stay Foolish", "Live with passion", "If you aim for the stars, at least you'll hit the moon", "The credit belongs to the man who is actually in the arena", "If you can't fly, then fun, if you can't run, then walk, if you can't walk, then crawl, but whatever you do, you have to keep moving forward", "Live as if you were to die tomorrow. Learn as if you were to live forever."
        **
        **
        "Brendon Burchard", "Steve Jobs", "Tony Robbins", "T. Harv Eker", "Theordore Roosevelt", "Dr. Martin Luther King Jr.", "Mahatma Gandhi"
    (2) Create an instance of this new class, initialize it
    (3) Using dot notation to access its quotes and authors. 
    (4) Can you get the first quote and author and assign them to two constants?
 
 */

class QuoteBook
{
    var quotes: [String] = []
    var authors: [String] = []
    
    init(newQuotes: [String], theirAuthors: [String])
    {
        quotes = newQuotes
        authors = theirAuthors
    }
}

let newQuotes = ["When you knock on the door of opportunities, don't be surprise that it is work who answers.", "Stay Hungry, Stay Foolish", "Live with passion", "If you aim for the stars, at least you'll hit the moon", "The credit belongs to the man who is actually in the arena", "If you can't fly, then fun, if you can't run, then walk, if you can't walk, then crawl, but whatever you do, you have to keep moving forward", "Live as if you were to die tomorrow. Learn as if you were to live forever."]

let theirAuthors = ["Brendon Burchard", "Steve Jobs", "Tony Robbins", "T. Harv Eker", "Theordore Roosevelt", "Dr. Martin Luther King Jr.", "Mahatma Gandhi"]


// now let's create the new quote book
var quoteBook = QuoteBook(newQuotes: newQuotes, theirAuthors: theirAuthors)

// great job! let's access this quotebook properties
quoteBook.quotes

// we can also access each quote inside this array
quoteBook.quotes[0]
quoteBook.quotes[1]

// the same for authors
quoteBook.authors[0]
quoteBook.authors[1]













