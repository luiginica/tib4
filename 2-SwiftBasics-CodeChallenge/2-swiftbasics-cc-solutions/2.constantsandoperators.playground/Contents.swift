
/**

 We are going to complete the tip calculator brain. Can you help me out?
 
 (1) Calculate the total bill amount
 (2) If we are sharing the bill, how much each person pays for this bill?
 (3) Sharing is caring! Share you answers and questions below this video. Let me know if you knocked down this challenge.
 
 * Hint: You can't devide a double by an integer in Swift or most other languages. Use Double(someInteger) to tell Swift that you want it to think of someInteger as a double. That's what we call "cast".
 
 */

var billBeforeTax: Double = 50.0
var taxPercentage: Double = 6.0
var numberOfPeople: Int = 2
let tipPercentage: Double = 15.0
// tipPercentage = 12.0         // ERROR

let taxAmount: Double = billBeforeTax * taxPercentage / 100.0
let totalBeforeTip: Double = billBeforeTax + taxAmount
let tipAmount: Double = totalBeforeTip * tipPercentage / 100.0

// ** Challenge:
let totalBill: Double = totalBeforeTip + tipAmount
let individualBill: Double = totalBill / Double(numberOfPeople)











// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.