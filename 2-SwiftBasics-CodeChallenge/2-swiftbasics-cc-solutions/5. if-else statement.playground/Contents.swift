
/**
 
 Ever log in to Facebook / Instagram or Snapchat - or even DucTran.co? 
 
 You'll have to enter your username and password, right? 
 
 Guess what Facebook has to check? Your entered username vs. the username they stored on their server. Yeah right, if-else!
 
 Here's your Code Challenge to make a social game authentication process:
 
 (1) Create a variable to store user's age. 
 (2) IF the user is younger than 13 years old, tell him that he must be older than 13 yr old to use this app.
 (3) Otherwise, create 4 constants for:
        entered username and password
        correct username and password
 
 Here comes the magic part! 
 
 (4) Compare:
        IF the entered username is the same as correct username
        AND if the entered password is the same as the correct password
        THEN tell the user he/she can play the game now
        OTHERWISE tell him/her that he/she just entered a wrong username or password.
 
 (5) Share you answer below with this HTML syntax:
        <code>
            yourCodeGoesHere
        </code
 
 (6) Check out the hints below and download the complete project too.
 
 * Hints:
    (1) You can nest if-else statement like this:
            if condition1 {
            
            } else {
                if condition2 {
                
                }
            }
    
    (2) When comparing the username, don't forget that username is case insensitive - meaning that it can be either lowercase or uppercase. To make sure a string is all lower cased, you can do exampleString.lowercased() - it will give you the lowercased version of exampleString.
    (3) To compare equality of two strings, use == logical operator
    (4) You can certainly nest yet another if-esle but there's a better way - using the AND operator. Example:
            (true && true)  is true
            (false && true) is false
            (true && false) is false
            (false && false) .. you guess
            
            This means that you can have condition1 && condition2 as a big condition in the if part.
    (5) Have fun!
 
 */

var age = 12

if age < 13 {
    print("You must be older than 13 years old to use this app.")
} else {
    // ask the user to enter username and password
    let enteredUsername = "DuctrongtRan"
    let enteredPassword = "test123"
    let correctUsername = "ductrongtran"
    let correctPassword = "test123"
    
    if enteredUsername.lowercased() == correctUsername && enteredPassword == correctPassword {
        print("Now you can join the game!")
    } else {
        print("Sorry, you just entered a wrong username or password")
    }
}









// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.














