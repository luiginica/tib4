
// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.

/**
 
 We are going to build a tip calculator. Let's lay out the infrastructure of it first.
 
 (1) Create 3 variables named in camel case of bill before tax, tax percentage, and number of people.
 
 (2) Assign these variables types: real number, real number, and integer respectively.
 
 (3) Give them some random initial values
 
 (4) Change all values of these variables.
 
 (5) Share your questions or answers below this video. Have fun!
 
 * Hint: camelCaseIsLikeThis
 
 
 */

import UIKit

var billBeforeTax: Double = 50.0
var taxPercentage: Double = 6.0
var numberOfPeople: Int = 2

billBeforeTax = 98.2
taxPercentage = 9.7
numberOfPeople = 3













