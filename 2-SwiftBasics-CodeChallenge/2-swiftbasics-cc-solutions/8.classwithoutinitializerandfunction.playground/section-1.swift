/**
 
 In module 2, we are going to create a motivational quote app. Why don't we cheat a bit and do some work in advance?
 
 I have some quotes you can copy & paste to your array:
    "When you knock on the door of opportunities, don't be surprise that it is work who answers.", "Stay Hungry, Stay Foolish", "Live with passion", "If you aim for the stars, at least you'll hit the moon", "The credit belongs to the man who is actually in the arena", "If you can't fly, then fun, if you can't run, then walk, if you can't walk, then crawl, but whatever you do, you have to keep moving forward", "Live as if you were to die tomorrow. Learn as if you were to live forever."
 And respectively here're the authrs of these quotes:
    "Brendon Burchard", "Steve Jobs", "Tony Robbins", "T. Harv Eker", "Theordore Roosevelt", "Dr. Martin Luther King Jr.", "Mahatma Gandhi"
 
 Here comes your CC:
    (1) Create a class called quote book - remember upper camel case?
    (2) Create two properties / member variables / instance variables (I'd like to call it properties - more descriptive!) named quotes and authors
    (3) Create an instance of the quote book
    (4) Assign new quotes and authors for this book using the data I gave you above.
    (5) Have fun and use <code>YourCode</code> to share your answer  below in the comment section.
 
 */

class QuoteBook
{
    var quotes: [String] = []
    var authors: [String] = []
}


var quoteBook = QuoteBook()
quoteBook.quotes = ["When you knock on the door of opportunities, don't be surprise that it is work who answers.", "Stay Hungry, Stay Foolish", "Live with passion", "If you aim for the stars, at least you'll hit the moon", "The credit belongs to the man who is actually in the arena", "If you can't fly, then fun, if you can't run, then walk, if you can't walk, then crawl, but whatever you do, you have to keep moving forward", "Live as if you were to die tomorrow. Learn as if you were to live forever."]
quoteBook.authors = ["Brendon Burchard", "Steve Jobs", "Tony Robbins", "T. Harv Eker", "Theordore Roosevelt", "Dr. Martin Luther King Jr.", "Mahatma Gandhi"]














