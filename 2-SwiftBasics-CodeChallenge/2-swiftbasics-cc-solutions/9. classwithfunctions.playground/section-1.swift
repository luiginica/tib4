import Foundation

/**
 
 Coming back to our QuoteBook class, can you add two new functionality of this to the game?
 
 (1) Write a method called getRandomQuote and give us a random quote - really random! You know how to do this! You got this!
 (2) Write a method called getAuthor, takes a quote and gives back the name of the quote's author. 
 
 * Hint: if you didn't figure this out, watch the solution video next to this one. We'll talk about it. But try to figure it out on your own first. Do some thinking & googling but not browsing! :)
 
 */

class QuoteBook
{
    var quotes: [String] = []
    var authors: [String] = []
    
    // function
    // method: means a function associated with a type (ex. class)
    func getRandomQuote() -> String
    {
        let quoteCount = quotes.count
        let randomIndex = Int(arc4random()) % quoteCount
        
        return quotes[randomIndex]
    }
    
    func getAuthor(of quote: String) -> String
    {
        if let index = quotes.index(of: quote) {
            return authors[index]
        }
        
        return ""
    }
}

let newQuotes = ["When you knock on the door of opportunities, don't be surprise that it is work who answers.", "Stay Hungry, Stay Foolish", "Live with passion", "If you aim for the stars, at least you'll hit the moon", "The credit belongs to the man who is actually in the arena", "If you can't fly, then fun, if you can't run, then walk, if you can't walk, then crawl, but whatever you do, you have to keep moving forward", "Live as if you were to die tomorrow. Learn as if you were to live forever."]

let theirAuthors = ["Brendon Burchard", "Steve Jobs", "Tony Robbins", "T. Harv Eker", "Theordore Roosevelt", "Dr. Martin Luther King Jr.", "Mahatma Gandhi"]

let quoteBook = QuoteBook()
quoteBook.quotes = newQuotes
quoteBook.authors = theirAuthors

let randomQuote = quoteBook.getRandomQuote()
let author = quoteBook.getAuthor(of: randomQuote)











