
/**
 
 This time we are telling a story. 
 
 (1) Declare five variables or constants for a name, verb, noun, adjective and adverb - don't forget to declare their types and give them some initial values
 (2) Tell the story by connecting together: [name] is a [adjective] [noun]
 (3) Create another story: [name] is always [adverb] [verb]
 
 * Hint: "story" means a string you are going to create. Q: how you can connect these together nicely?
 
 */

var name: String = "Duc"
var verb: String = "knows"
var noun: String = "Swift"
var adjective: String = "smart"
var adverb = "hardworkingly"

let story = "\(name) is a \(adjective) \(noun)"
let anotherStory = "They are always \(adverb) \(verb)"




// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.






