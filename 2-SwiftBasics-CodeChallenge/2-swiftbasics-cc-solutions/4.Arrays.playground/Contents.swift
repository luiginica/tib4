/**
 
 Since array is so important - believe me, you won't be able to find an app that doesn't use array INTENSIVELY - like everywhere! 
 
 You'll see!
 
 So we will have two Code Challenges today
 
 (1) Create an array to hold 5 numbers: 100, 98, 80, 83, 23
 (2) Name this array "grades" - who scores 23? 
 (3) Calculate the total grades of this guy
 (4) Calculate the average grade
 
 --
 
 (5) Create a contacts array with these names: "Duc", "Tracy", "Brendon", "Chalene", "Miley"
 (6) Find out how many contacts in this list - don't count by hand. Let Swift does its magin.
 (7) We just got to know Michele and Lisa. How about you add their names to our contacts list?
 
 Have fun!!!
 
 */


// ** Challenge
var grades = [100, 98, 80, 83]
let totalGrade = grades[0] + grades[1] + grades[2] + grades[3]

let name1 = "Duc Tran"
let transcript = "\(name1) has a total grade of \(totalGrade)"
let numberOfGrades = grades.count
let averageGrade = Double(totalGrade) / Double(numberOfGrades)

// ** Challenge 2
var contacts = ["Duc", "Tracy", "Brendon", "Chalene", "Miley"]

let numberOfContacts = contacts.count
let firstContact = contacts[0]

let lastIndex = numberOfContacts - 1
let lastContact = contacts[lastIndex]

contacts.append("Michelle")
contacts.append("Lisa")




// Created by Duc Tran
// http://ductran.io
// This material and all content within this document are copyrighted and based on propriety concepts from Duc Tran's Total iOS Blueprint program. Do not duplicate, distribute, publish, share, or train from without written permission. For inquiries, contact support@ductran.co
// © 2014-Current The Tran Group. All rights reserved.




















